//
//  main.m
//  CodeSampleTest
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KVAppDelegate class]));
    }
}
