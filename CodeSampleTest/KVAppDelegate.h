//
//  KVAppDelegate.h
//  CodeSampleTest
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
