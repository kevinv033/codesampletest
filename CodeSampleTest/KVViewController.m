//
//  KVViewController.m
//  CodeSampleTest
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import "KVViewController.h"

#define DEBUGX

@interface KVViewController ()

@end

@implementation KVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    formManager = [[KVFormManager alloc] init];
    
    //Listen for dictionary populating
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logData:) name:@"DictionaryPopulated" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

-(void)storeDataTapped:(id)sender
{
    
    // Static data to send to KVFormManager Object
    NSArray *keysArray = [NSArray arrayWithObjects:@"Name", @"Email", @"Phone", nil];
    NSArray *valuesArray = [NSArray arrayWithObjects:@"Kevin Vertucio", @"kevin.vertucio@gmail.com",@"67-940-7427", nil];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
    // Initialize the dictionary
    [formManager initWithDictionay:dictionary];

}

-(void)showViewTapped:(id)sender
{

    UIStoryboard *storyboard = self.storyboard;
    UIViewController *emptyViewController = [storyboard instantiateViewControllerWithIdentifier:@"EmptyViewController"];
    emptyViewController.view.backgroundColor = [UIColor orangeColor];
    
    [self presentViewController:emptyViewController animated:YES completion:nil];
    
}

#pragma mark - Instance Methods

-(void)logData:(NSNotification *)notification
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"UserInfo: %@", notification.userInfo);
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DictionaryPopulated" object:nil];
    
}

@end
