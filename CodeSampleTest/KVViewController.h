//
//  KVViewController.h
//  CodeSampleTest
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVFormManager.h"

@interface KVViewController : UIViewController
{
    KVFormManager *formManager;
}

@property (strong) IBOutlet UIButton *showViewButton;
@property (strong) IBOutlet UIButton *storeDataButton;

//This method creates a dictionary and sends it to KVFormManager
- (IBAction)storeDataTapped:(id)sender;
//This method presents the second viewcontroller in the main storyboard
- (IBAction)showViewTapped:(id)sender;

//This notfication handler logs the data in the console if DEBUGX is defined
- (void)logData:(NSNotification *)notification;

@end
