//
//  KVFormManager.h
//  CodeSampleTest
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>
@interface KVFormManager : NSObject
{
    dispatch_queue_t bgdQueue;
}

@property (strong) NSDictionary *theDictionary;


-(id)init;
// Method to populate the dictionary
-(void)initWithDictionay:(NSDictionary *)dictionary;



@end
