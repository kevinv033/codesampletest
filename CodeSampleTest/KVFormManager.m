//
//  KVFormManager.m
//  CodeSampleTest
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import "KVFormManager.h"

@implementation KVFormManager

- (id)init
{
    self = [super init];
    if (self) {
        //initialization
    }
    return self;
}

-(void)initWithDictionay:(NSDictionary *)dictionary
{
    //create a thread to perform data population
    bgdQueue = dispatch_queue_create("com.kevinv033.ios.app.codesample.queue", NULL);
    dispatch_async(bgdQueue, ^(void) { self.theDictionary = dictionary; } ) ;
    
    //Post a notification that dictionary is being populated
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DictionaryPopulated" object:self userInfo:dictionary];
    
}

@end
