//
//  CodeSampleTestTests.m
//  CodeSampleTestTests
//
//  Created by Kevin Vertucio on 09/19/13.
//  Copyright (c) 2013 Kevin Vertucio. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface CodeSampleTestTests : XCTestCase

@end

@implementation CodeSampleTestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
